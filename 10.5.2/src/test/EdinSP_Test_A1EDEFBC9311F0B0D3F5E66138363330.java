/**@generated
 * WARNING � Changes you make to this file may be lost.
 *           File is generated and may be re-generated without warning.
 * @RPT-Core-generated Version 8.6
 */
/******************************************************************
* Licensed Materials - Property of IBM and/or HCL
* Copyright IBM Corporation 2013, 2017. All Rights Reserved.
* Copyright HCL Technologies Ltd. 2017, 2020.  All Rights Reserved.
* 
* U.S. Government Users Restricted Rights - Use, duplication or 
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
******************************************************************/
/* TestScript.template - Suitable for collaborative code generation */
package test;

import com.ibm.rational.test.lt.datacorrelation.execution.action.VariableAction;
import com.ibm.rational.test.lt.datacorrelation.execution.harvest.DataCorrelationVar;
import com.ibm.rational.test.lt.datacorrelation.execution.harvest.DataVar;
import com.ibm.rational.test.lt.datacorrelation.execution.harvest.IDCCoreVar;
import com.ibm.rational.test.lt.datacorrelation.execution.harvest.IDataCorrelationVar;
import com.ibm.rational.test.lt.datacorrelation.execution.proto.ProtoAdapterHandler;
import com.ibm.rational.test.lt.execution.core.impl.LTTestScript;
import com.ibm.rational.test.lt.execution.moeb.action.EndMoebStepBatchAction;
import com.ibm.rational.test.lt.execution.moeb.action.MoebStepBatchAction;
import com.ibm.rational.test.lt.execution.moeb.services.*;
import com.ibm.rational.test.lt.execution.moeb.services.MoebActionFailedEvent;
import com.ibm.rational.test.lt.execution.moeb.services.MoebFatalErrorEvent;
import com.ibm.rational.test.lt.kernel.IDataArea;
import com.ibm.rational.test.lt.kernel.action.IContainer;
import com.ibm.rational.test.lt.kernel.action.impl.KThrow;
import com.ibm.rational.test.lt.kernel.services.*;
import com.ibm.rational.test.lt.kernel.services.RPTEventStructure;

@SuppressWarnings("all")
public class EdinSP_Test_A1EDEFBC9311F0B0D3F5E66138363330 extends LTTestScript  {

    static ProtoAdapterHandler pa = new ProtoAdapterHandler();
    static {           
        pa.addPA("com.ibm.rational.test.lt.datacorrelation.execution.protocol.core.CoreProtoAdapter", "coreAdapter");
pa.addPA("com.ibm.rational.test.lt.execution.moeb.dc.MoebDataAdapter", "com.ibm.rational.test.lt.execution.moeb.action.MoebStepBatchAction");
    }
	
	private DataVar[] vars = new DataVar[1];
	
    
    
	
	public EdinSP_Test_A1EDEFBC9311F0B0D3F5E66138363330(IContainer container, String invocationId) {
		super(container, "EdinSP", invocationId, "A1EDEFBC9311F0B0D3F5E66138363330");
		setArmEnabled(false);
		stopAtYourConvenience=false;
		
	}

    public void execute() {
    	try {
			
	    		if (!isScheduleRun()) setThinkMax(2000);
	
	if (!isScheduleRun()){
this.addEventBehavior(new RPTEventStructure(new RPTFailVPEvent(), new RPTContinueEvent("Content Verification Point failed"), 1, "Content Verification Point failed"));
	this.addEventBehavior(new RPTEventStructure(new RPTConnectEvent(), new RPTContinueEvent("Connection failed"), 1, "Connection failed"));
	this.addEventBehavior(new RPTEventStructure(new RPTAuthenticationEvent(), new RPTContinueEvent("Authentication failed"), 1, "Authentication failed"));
	this.addEventBehavior(new RPTEventStructure(new RPTDataPoolEOFEvent(), new RPTStopUserEvent("End of dataset reached"), 1, "End of dataset reached"));
	this.addEventBehavior(new RPTEventStructure(new RPTReferenceEvent(), new RPTContinueEvent("Failed to extract reference"), 1, "Failed to extract reference"));
	this.addEventBehavior(new RPTEventStructure(new RPTSubstitutionEvent(), new RPTContinueEvent("Substitution failed"), 1, "Substitution failed"));
	this.addEventBehavior(new RPTEventStructure(new RPTServerTimeoutEvent(), new RPTContinueEvent("Timeout"), 1, "Timeout"));
	this.addEventBehavior(new RPTEventStructure(new RPTDroppedConnectionEvent(), new RPTContinueEvent("Server dropped connection"), 1, "Server dropped connection"));
	this.addEventBehavior(new RPTEventStructure(new RPTCustomCodeVPFailureEvent(), new RPTContinueEvent("Custom Verification Point failed"), 1, "Custom Verification Point failed"));
	this.addEventBehavior(new RPTEventStructure(new RPTCustomCodeAlertEvent(), new RPTContinueEvent("Custom Code reported an Alert"), 0, "Custom Code reported an Alert"));
	this.addEventBehavior(new RPTEventStructure(new RPTCustomCodeExceptionEvent(), new RPTStopUserEvent("Custom Code reported an unhandled exception"), 1, "Custom Code reported an unhandled exception"));
	this.addEventBehavior(new RPTEventStructure(new MoebActionFailedEvent(), new RPTContinueEvent("Playback of UI Action failed"), 0, "Playback of UI Action failed"));
	this.addEventBehavior(new RPTEventStructure(new MoebFatalErrorEvent(), new RPTStopUserEvent("Playback of UI step has a fatal error"), 0, "Playback of UI step has a fatal error"));
	}
	
	
	        	this.add(varAction_1(this));
	this.add(applicationContext_1(this));
	this.add(applicationContext_2(this));
	this.add(applicationContext_3(this));
	this.add(applicationContext_4(this));
	this.add(applicationContext_5(this));
	this.add(applicationContext_6(this));
	this.add(applicationContext_7(this));
			this.addFinally(new EndMoebStepBatchAction(this));

	    } catch (Throwable e) {
		     log("Test Execution: EdinSP_Test_A1EDEFBC9311F0B0D3F5E66138363330 ",e);
	    } finally {
	        super.execute();
		}	    
    }
    
	public void preFinish() {
		
		super.preFinish();
	}
	
	public void stop() {
		
		super.stop();
	}	
	
	private VariableAction varAction_1(final IContainer parent) {

	VariableAction vc = new VariableAction(parent, "", "A1EDEFBC931E73D3D3F5E66138363330");	
			vars[0] = new DataVar("RTW_WebUI_Browser_Selection", "Firefox", IDataArea.VIRTUALUSER, "IGNORE", false, false);

		
	vc.add(vars);
	return vc;
	
}
	// MoebStepBatch_Decl.template
	public MoebStepBatchAction applicationContext_1(IContainer parent) {
		MoebStepBatchAction stepBatch = new MoebStepBatchAction(parent,
												  "Start VB_Bridged",
												  "A1EDEFBC93315F90D3F5E66138363330",
												  "{\"id\":\"0\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceTestSteps\",\"steps\":[{\"description\":\"Start <b>VB_Bridged<\\/b>\",\"to_overriden\":false,\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0,\"optionalStep\":false,\"uid\":\"A1EDEFBC93315F90D3F5E66138363330\",\"application_package\":\"com.ibm.rational.test.mobile.android.browser\",\"application_name\":\"VB_Bridged\",\"application_os\":\"WebUI\",\"isWeb\":false,\"action\":{\"id\":\"2\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceAction\",\"type\":\"start\",\"parameters\":[{\"name\":\"activity\",\"id\":\"3\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"com.ibm.rational.test.mobile.android.browser.BrowserMainActivity\"},{\"name\":\"rmot_kind_of_launch\",\"id\":\"4\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"playback\"},{\"name\":\"starting_url\",\"id\":\"5\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"http:\\/\\/192.168.0.140:8089\\/vbooking-home\\/\"}]},\"sync_policy\":\"DEFAULT\",\"id\":\"1\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceHWActionStep\"}],\"timeout\":10}",
												  "/10.5.2/EdinSP.testsuite",
												  null,
												  "<default>");



		return stepBatch;
	}

	// MoebStepBatch_Decl.template
	public MoebStepBatchAction applicationContext_2(IContainer parent) {
		MoebStepBatchAction stepBatch = new MoebStepBatchAction(parent,
												  "Click on Submit button whose Content is Edinburgh - VB_Bridged",
												  "A1EDEFBC9346BC54D3F5E66138363330",
												  "{\"id\":\"0\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceTestSteps\",\"steps\":[{\"revealPref\":\"true\",\"description\":\"Click on <b>Submit button<\\/b> whose <b>Content<\\/b> is <b>Edinburgh<\\/b>\",\"to_overriden\":false,\"skipScrollingIniOSDF\":false,\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0,\"screenshotPref\":\"ALL\",\"optionalStep\":false,\"uid\":\"A1EDEFBC9346BC54D3F5E66138363330\",\"isWeb\":true,\"action\":{\"id\":\"2\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceAction\",\"type\":\"onclick\",\"parameters\":[{\"name\":\"enableasyncaction\",\"id\":\"3\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TBoolean\",\"value\":\"false\"}]},\"sync_policy\":\"DEFAULT\",\"id\":\"1\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIActionStep\",\"object\":{\"identifier\":{\"parameter\":{\"name\":\"content\",\"id\":\"6\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"Edinburgh\"},\"name\":\"content\",\"id\":\"5\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceId\",\"operator\":\"TEquals\"},\"propList\":\"{ \\\"Coordinates$array$\\\": \\\"top:504;left:788;bottom:709;right:1127\\\", \\\"Geometry\\\": { \\\"height\\\": 205, \\\"width\\\": 338, \\\"x\\\": 788, \\\"y\\\": 504 }, \\\"Style$array$\\\": \\\"background-color:rgba(0, 0, 0, 0);background-repeat:no-repeat;color:rgb(50, 50, 50);font-family:MS Shell Dlg \\\\\\\\32 ;font-size:28.8px;font-style:normal;font-weight:400;line-height:normal;margin-bottom:0px;margin-left:0px;margin-right:0px;margin-top:0px;opacity:1;padding-bottom:160px;padding-left:30px;padding-right:180px;padding-top:10px;text-align:center;text-decoration:rgb(50, 50, 50);visibility:visible;z-index:auto;\\\", \\\"background-color\\\": \\\"00000000\\\", \\\"background-repeat\\\": \\\"no-repeat\\\", \\\"bottom\\\": 709, \\\"class\\\": \\\"trans_btn\\\", \\\"color\\\": \\\"323232FF\\\", \\\"content\\\": \\\"Edinburgh\\\", \\\"domainName\\\": \\\"html\\\", \\\"enabled\\\": true, \\\"exist\\\": true, \\\"font-family\\\": \\\"MS Shell Dlg \\\\\\\\32 \\\", \\\"font-size\\\": \\\"28.8\\\", \\\"font-style\\\": \\\"normal\\\", \\\"font-weight\\\": \\\"400\\\", \\\"left\\\": 788, \\\"line-height\\\": \\\"normal\\\", \\\"margin-bottom\\\": 0, \\\"margin-left\\\": 0, \\\"margin-right\\\": 0, \\\"margin-top\\\": 0, \\\"name\\\": \\\"submit\\\", \\\"opacity\\\": \\\"1\\\", \\\"padding-bottom\\\": 160, \\\"padding-left\\\": 30, \\\"padding-right\\\": 180, \\\"padding-top\\\": 10, \\\"proxyClass\\\": \\\"HtmlSubmitProxy\\\", \\\"proxyName\\\": \\\"inputsubmit\\\", \\\"right\\\": 1127, \\\"tagname\\\": \\\"input\\\", \\\"text-align\\\": \\\"center\\\", \\\"text-decoration\\\": \\\"323232FF\\\", \\\"top\\\": 504, \\\"type\\\": \\\"SUBMIT\\\", \\\"value\\\": \\\"Edinburgh\\\", \\\"visibility\\\": \\\"visible\\\", \\\"visible\\\": true, \\\"xpath\\\": \\\"151\\\", \\\"xpathProp\\\": \\\"\\\\\\/\\\\\\/html\\\\\\/body\\\\\\/div\\\\\\/table[2]\\\\\\/tbody\\\\\\/tr\\\\\\/td[2]\\\\\\/form\\\\\\/input[11]\\\", \\\"z-index\\\": \\\"auto\\\" }\",\"id\":\"4\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIObject\",\"type\":\"html.inputsubmit\"}}],\"timeout\":10}",
												  "/10.5.2/EdinSP.testsuite",
												  null,
												  "<default>");



		return stepBatch;
	}

	// MoebStepBatch_Decl.template
	public MoebStepBatchAction applicationContext_3(IContainer parent) {
		MoebStepBatchAction stepBatch = new MoebStepBatchAction(parent,
												  "Select 'Ling' in Drop-down list whose Content is Autofill Customer - VB_Bridged",
												  "A1EDEFBC939CCBA2D3F5E66138363330",
												  "{\"id\":\"0\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceTestSteps\",\"steps\":[{\"optionalStep\":false,\"uid\":\"A1EDEFBC939CCBA2D3F5E66138363330\",\"preferredThinktime\":0,\"thinktime\":7160,\"isWeb\":false,\"description\":\"Think <i>7,160<\\/i> ms\",\"id\":\"1\",\"to_overriden\":false,\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceThinkTime\",\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0},{\"revealPref\":\"true\",\"description\":\"Select '<b>Ling<\\/b>' in <b>Drop-down list<\\/b> whose <b>Content<\\/b> is <b>Autofill Customer<\\/b>\",\"to_overriden\":false,\"skipScrollingIniOSDF\":false,\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0,\"screenshotPref\":\"ALL\",\"optionalStep\":false,\"uid\":\"A1EDEFBC939CCBA2D3F5E66138363330\",\"isWeb\":true,\"action\":{\"id\":\"3\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceAction\",\"type\":\"onchange\",\"parameters\":[{\"name\":\"newtext\",\"id\":\"4\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"Ling\"}]},\"sync_policy\":\"DEFAULT\",\"id\":\"2\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIActionStep\",\"object\":{\"identifier\":{\"parameter\":{\"name\":\"content\",\"id\":\"7\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"Autofill Customer\"},\"name\":\"content\",\"id\":\"6\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceId\",\"operator\":\"TEquals\"},\"propList\":\"{ \\\"Coordinates$array$\\\": \\\"top:280;left:801;bottom:308;right:951\\\", \\\"Geometry\\\": { \\\"height\\\": 28, \\\"width\\\": 150, \\\"x\\\": 801, \\\"y\\\": 280 }, \\\"Style$array$\\\": \\\"background-color:rgb(0, 58, 140);background-repeat:no-repeat;color:rgb(255, 255, 255);font-family:MS Shell Dlg \\\\\\\\32 ;font-size:14.4px;font-style:normal;font-weight:400;line-height:normal;margin-bottom:5px;margin-left:20px;margin-right:10px;margin-top:0px;opacity:1;padding-bottom:1px;padding-left:4px;padding-right:4px;padding-top:1px;text-align:start;text-decoration:rgb(255, 255, 255);visibility:visible;z-index:auto;\\\", \\\"background-color\\\": \\\"003A8CFF\\\", \\\"background-repeat\\\": \\\"no-repeat\\\", \\\"bottom\\\": 308, \\\"class\\\": \\\"booked_set\\\", \\\"color\\\": \\\"FFFFFFFF\\\", \\\"content\\\": \\\"Autofill Customer\\\", \\\"domainName\\\": \\\"html\\\", \\\"enabled\\\": true, \\\"exist\\\": true, \\\"font-family\\\": \\\"MS Shell Dlg \\\\\\\\32 \\\", \\\"font-size\\\": \\\"14.4\\\", \\\"font-style\\\": \\\"normal\\\", \\\"font-weight\\\": \\\"400\\\", \\\"id\\\": \\\"passengerSelector\\\", \\\"left\\\": 801, \\\"length\\\": 4, \\\"line-height\\\": \\\"normal\\\", \\\"margin-bottom\\\": 5, \\\"margin-left\\\": 20, \\\"margin-right\\\": 10, \\\"margin-top\\\": 0, \\\"name\\\": \\\"\\\", \\\"opacity\\\": \\\"1\\\", \\\"options\\\": \\\"Autofill Customer, David, Ling, Rohit\\\", \\\"padding-bottom\\\": 1, \\\"padding-left\\\": 4, \\\"padding-right\\\": 4, \\\"padding-top\\\": 1, \\\"proxyClass\\\": \\\"HtmlSelectProxy\\\", \\\"proxyName\\\": \\\"select\\\", \\\"right\\\": 951, \\\"tagname\\\": \\\"select\\\", \\\"text-align\\\": \\\"start\\\", \\\"text-decoration\\\": \\\"FFFFFFFF\\\", \\\"top\\\": 280, \\\"visibility\\\": \\\"visible\\\", \\\"visible\\\": true, \\\"xpath\\\": \\\"153\\\", \\\"xpathProp\\\": \\\"\\\\\\/\\\\\\/select[@id='passengerSelector']\\\", \\\"z-index\\\": \\\"auto\\\" }\",\"id\":\"5\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIObject\",\"type\":\"html.select\"}}],\"timeout\":10}",
												  "/10.5.2/EdinSP.testsuite",
												  null,
												  "<default>");



		return stepBatch;
	}

	// MoebStepBatch_Decl.template
	public MoebStepBatchAction applicationContext_4(IContainer parent) {
		MoebStepBatchAction stepBatch = new MoebStepBatchAction(parent,
												  "Click on Submit button whose Content is PROCEED - VB_Bridged",
												  "A1EDEFBC93BB7724D3F5E66138363330",
												  "{\"id\":\"0\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceTestSteps\",\"steps\":[{\"optionalStep\":false,\"uid\":\"A1EDEFBC93BB7724D3F5E66138363330\",\"preferredThinktime\":0,\"thinktime\":1569,\"isWeb\":false,\"description\":\"Think <i>1,569<\\/i> ms\",\"id\":\"1\",\"to_overriden\":false,\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceThinkTime\",\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0},{\"revealPref\":\"true\",\"description\":\"Click on <b>Submit button<\\/b> whose <b>Content<\\/b> is <b>PROCEED<\\/b>\",\"to_overriden\":false,\"skipScrollingIniOSDF\":false,\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0,\"screenshotPref\":\"ALL\",\"optionalStep\":false,\"uid\":\"A1EDEFBC93BB7724D3F5E66138363330\",\"isWeb\":true,\"action\":{\"id\":\"3\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceAction\",\"type\":\"onclick\",\"parameters\":[{\"name\":\"enableasyncaction\",\"id\":\"4\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TBoolean\",\"value\":\"false\"}]},\"sync_policy\":\"DEFAULT\",\"id\":\"2\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIActionStep\",\"object\":{\"identifier\":{\"parameter\":{\"name\":\"content\",\"id\":\"7\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"PROCEED\"},\"name\":\"content\",\"id\":\"6\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceId\",\"operator\":\"TEquals\"},\"propList\":\"{ \\\"Coordinates$array$\\\": \\\"top:555;left:1159;bottom:583;right:1275\\\", \\\"Geometry\\\": { \\\"height\\\": 28, \\\"width\\\": 116, \\\"x\\\": 1159, \\\"y\\\": 555 }, \\\"Style$array$\\\": \\\"background-color:rgb(160, 160, 160);background-repeat:repeat;color:rgb(255, 255, 255);font-family:MS Shell Dlg \\\\\\\\32 ;font-size:14.4px;font-style:normal;font-weight:400;line-height:normal;margin-bottom:0px;margin-left:20px;margin-right:0px;margin-top:0px;opacity:1;padding-bottom:1px;padding-left:4px;padding-right:4px;padding-top:1px;text-align:center;text-decoration:rgb(255, 255, 255);visibility:visible;z-index:auto;\\\", \\\"background-color\\\": \\\"A0A0A0FF\\\", \\\"background-repeat\\\": \\\"repeat\\\", \\\"bottom\\\": 583, \\\"class\\\": \\\"btn\\\", \\\"color\\\": \\\"FFFFFFFF\\\", \\\"content\\\": \\\"PROCEED\\\", \\\"domainName\\\": \\\"html\\\", \\\"enabled\\\": true, \\\"exist\\\": true, \\\"font-family\\\": \\\"MS Shell Dlg \\\\\\\\32 \\\", \\\"font-size\\\": \\\"14.4\\\", \\\"font-style\\\": \\\"normal\\\", \\\"font-weight\\\": \\\"400\\\", \\\"left\\\": 1159, \\\"line-height\\\": \\\"normal\\\", \\\"margin-bottom\\\": 0, \\\"margin-left\\\": 20, \\\"margin-right\\\": 0, \\\"margin-top\\\": 0, \\\"opacity\\\": \\\"1\\\", \\\"padding-bottom\\\": 1, \\\"padding-left\\\": 4, \\\"padding-right\\\": 4, \\\"padding-top\\\": 1, \\\"proxyClass\\\": \\\"HtmlSubmitProxy\\\", \\\"proxyName\\\": \\\"inputsubmit\\\", \\\"right\\\": 1275, \\\"tagname\\\": \\\"input\\\", \\\"text-align\\\": \\\"center\\\", \\\"text-decoration\\\": \\\"FFFFFFFF\\\", \\\"top\\\": 555, \\\"type\\\": \\\"SUBMIT\\\", \\\"value\\\": \\\"PROCEED\\\", \\\"visibility\\\": \\\"visible\\\", \\\"visible\\\": true, \\\"xpath\\\": \\\"259\\\", \\\"xpathProp\\\": \\\"\\\\\\/\\\\\\/form[@id='bookingForm']\\\\\\/table\\\\\\/tbody\\\\\\/tr\\\\\\/td[3]\\\\\\/table\\\\\\/tbody\\\\\\/tr\\\\\\/td\\\\\\/input[7]\\\", \\\"z-index\\\": \\\"auto\\\" }\",\"id\":\"5\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIObject\",\"type\":\"html.inputsubmit\"}}],\"timeout\":10}",
												  "/10.5.2/EdinSP.testsuite",
												  null,
												  "<default>");



		return stepBatch;
	}

	// MoebStepBatch_Decl.template
	public MoebStepBatchAction applicationContext_5(IContainer parent) {
		MoebStepBatchAction stepBatch = new MoebStepBatchAction(parent,
												  "Click on Submit button whose Content is SEARCH - VB_Bridged",
												  "A1EDEFBC93D5B5F2D3F5E66138363330",
												  "{\"id\":\"0\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceTestSteps\",\"steps\":[{\"optionalStep\":false,\"uid\":\"A1EDEFBC93D5B5F2D3F5E66138363330\",\"preferredThinktime\":0,\"thinktime\":4524,\"isWeb\":false,\"description\":\"Think <i>4,524<\\/i> ms\",\"id\":\"1\",\"to_overriden\":false,\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceThinkTime\",\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0},{\"revealPref\":\"true\",\"description\":\"Click on <b>Submit button<\\/b> whose <b>Content<\\/b> is <b>SEARCH<\\/b>\",\"to_overriden\":false,\"skipScrollingIniOSDF\":false,\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0,\"screenshotPref\":\"ALL\",\"optionalStep\":false,\"uid\":\"A1EDEFBC93D5B5F2D3F5E66138363330\",\"isWeb\":true,\"action\":{\"id\":\"3\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceAction\",\"type\":\"onclick\",\"parameters\":[{\"name\":\"enableasyncaction\",\"id\":\"4\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TBoolean\",\"value\":\"false\"}]},\"sync_policy\":\"DEFAULT\",\"id\":\"2\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIActionStep\",\"object\":{\"identifier\":{\"parameter\":{\"name\":\"content\",\"id\":\"7\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"SEARCH\"},\"name\":\"content\",\"id\":\"6\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceId\",\"operator\":\"TEquals\"},\"propList\":\"{ \\\"Coordinates$array$\\\": \\\"top:310;left:1115;bottom:338;right:1231\\\", \\\"Geometry\\\": { \\\"height\\\": 28, \\\"width\\\": 116, \\\"x\\\": 1115, \\\"y\\\": 310 }, \\\"Style$array$\\\": \\\"background-color:rgb(160, 160, 160);background-repeat:repeat;color:rgb(255, 255, 255);font-family:MS Shell Dlg \\\\\\\\32 ;font-size:14.4px;font-style:normal;font-weight:400;line-height:normal;margin-bottom:20px;margin-left:20px;margin-right:0px;margin-top:0px;opacity:1;padding-bottom:1px;padding-left:4px;padding-right:4px;padding-top:1px;text-align:center;text-decoration:rgb(255, 255, 255);visibility:visible;z-index:auto;\\\", \\\"background-color\\\": \\\"A0A0A0FF\\\", \\\"background-repeat\\\": \\\"repeat\\\", \\\"bottom\\\": 338, \\\"class\\\": \\\"btn\\\", \\\"color\\\": \\\"FFFFFFFF\\\", \\\"content\\\": \\\"SEARCH\\\", \\\"domainName\\\": \\\"html\\\", \\\"enabled\\\": true, \\\"exist\\\": true, \\\"font-family\\\": \\\"MS Shell Dlg \\\\\\\\32 \\\", \\\"font-size\\\": \\\"14.4\\\", \\\"font-style\\\": \\\"normal\\\", \\\"font-weight\\\": \\\"400\\\", \\\"left\\\": 1115, \\\"line-height\\\": \\\"normal\\\", \\\"margin-bottom\\\": 20, \\\"margin-left\\\": 20, \\\"margin-right\\\": 0, \\\"margin-top\\\": 0, \\\"opacity\\\": \\\"1\\\", \\\"padding-bottom\\\": 1, \\\"padding-left\\\": 4, \\\"padding-right\\\": 4, \\\"padding-top\\\": 1, \\\"proxyClass\\\": \\\"HtmlSubmitProxy\\\", \\\"proxyName\\\": \\\"inputsubmit\\\", \\\"right\\\": 1231, \\\"tagname\\\": \\\"input\\\", \\\"text-align\\\": \\\"center\\\", \\\"text-decoration\\\": \\\"FFFFFFFF\\\", \\\"top\\\": 310, \\\"type\\\": \\\"submit\\\", \\\"value\\\": \\\"SEARCH\\\", \\\"visibility\\\": \\\"visible\\\", \\\"visible\\\": true, \\\"xpath\\\": \\\"71\\\", \\\"xpathProp\\\": \\\"\\\\\\/\\\\\\/html\\\\\\/body\\\\\\/div\\\\\\/table\\\\\\/tbody\\\\\\/tr[2]\\\\\\/td[2]\\\\\\/div\\\\\\/div[2]\\\\\\/form\\\\\\/input[4]\\\", \\\"z-index\\\": \\\"auto\\\" }\",\"id\":\"5\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIObject\",\"type\":\"html.inputsubmit\"}}],\"timeout\":10}",
												  "/10.5.2/EdinSP.testsuite",
												  null,
												  "<default>");



		return stepBatch;
	}

	// MoebStepBatch_Decl.template
	public MoebStepBatchAction applicationContext_6(IContainer parent) {
		MoebStepBatchAction stepBatch = new MoebStepBatchAction(parent,
												  "Select 'Ling' in Drop-down list whose Content is Autofill Customer - VB_Bridged",
												  "A1EDEFBC93E8C8B4D3F5E66138363330",
												  "{\"id\":\"0\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceTestSteps\",\"steps\":[{\"optionalStep\":false,\"uid\":\"A1EDEFBC93E8C8B4D3F5E66138363330\",\"preferredThinktime\":0,\"thinktime\":7107,\"isWeb\":false,\"description\":\"Think <i>7,107<\\/i> ms\",\"id\":\"1\",\"to_overriden\":false,\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceThinkTime\",\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0},{\"revealPref\":\"true\",\"description\":\"Select '<b>Ling<\\/b>' in <b>Drop-down list<\\/b> whose <b>Content<\\/b> is <b>Autofill Customer<\\/b>\",\"to_overriden\":false,\"skipScrollingIniOSDF\":false,\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0,\"screenshotPref\":\"ALL\",\"optionalStep\":false,\"uid\":\"A1EDEFBC93E8C8B4D3F5E66138363330\",\"isWeb\":true,\"action\":{\"id\":\"3\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceAction\",\"type\":\"onchange\",\"parameters\":[{\"name\":\"newtext\",\"id\":\"4\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"Ling\"}]},\"sync_policy\":\"DEFAULT\",\"id\":\"2\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIActionStep\",\"object\":{\"identifier\":{\"parameter\":{\"name\":\"content\",\"id\":\"7\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"Autofill Customer\"},\"name\":\"content\",\"id\":\"6\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceId\",\"operator\":\"TEquals\"},\"propList\":\"{ \\\"Coordinates$array$\\\": \\\"top:180;left:801;bottom:208;right:951\\\", \\\"Geometry\\\": { \\\"height\\\": 28, \\\"width\\\": 150, \\\"x\\\": 801, \\\"y\\\": 180 }, \\\"Style$array$\\\": \\\"background-color:rgb(0, 58, 140);background-repeat:no-repeat;color:rgb(255, 255, 255);font-family:MS Shell Dlg \\\\\\\\32 ;font-size:14.4px;font-style:normal;font-weight:400;line-height:normal;margin-bottom:5px;margin-left:20px;margin-right:10px;margin-top:0px;opacity:1;padding-bottom:1px;padding-left:4px;padding-right:4px;padding-top:1px;text-align:start;text-decoration:rgb(255, 255, 255);visibility:visible;z-index:auto;\\\", \\\"background-color\\\": \\\"003A8CFF\\\", \\\"background-repeat\\\": \\\"no-repeat\\\", \\\"bottom\\\": 208, \\\"class\\\": \\\"booked_set\\\", \\\"color\\\": \\\"FFFFFFFF\\\", \\\"content\\\": \\\"Autofill Customer\\\", \\\"domainName\\\": \\\"html\\\", \\\"enabled\\\": true, \\\"exist\\\": true, \\\"font-family\\\": \\\"MS Shell Dlg \\\\\\\\32 \\\", \\\"font-size\\\": \\\"14.4\\\", \\\"font-style\\\": \\\"normal\\\", \\\"font-weight\\\": \\\"400\\\", \\\"id\\\": \\\"passengerSelector\\\", \\\"left\\\": 801, \\\"length\\\": 4, \\\"line-height\\\": \\\"normal\\\", \\\"margin-bottom\\\": 5, \\\"margin-left\\\": 20, \\\"margin-right\\\": 10, \\\"margin-top\\\": 0, \\\"name\\\": \\\"\\\", \\\"opacity\\\": \\\"1\\\", \\\"options\\\": \\\"Autofill Customer, David, Ling, Rohit\\\", \\\"padding-bottom\\\": 1, \\\"padding-left\\\": 4, \\\"padding-right\\\": 4, \\\"padding-top\\\": 1, \\\"proxyClass\\\": \\\"HtmlSelectProxy\\\", \\\"proxyName\\\": \\\"select\\\", \\\"right\\\": 951, \\\"tagname\\\": \\\"select\\\", \\\"text-align\\\": \\\"start\\\", \\\"text-decoration\\\": \\\"FFFFFFFF\\\", \\\"top\\\": 180, \\\"visibility\\\": \\\"visible\\\", \\\"visible\\\": true, \\\"xpath\\\": \\\"179\\\", \\\"xpathProp\\\": \\\"\\\\\\/\\\\\\/select[@id='passengerSelector']\\\", \\\"z-index\\\": \\\"auto\\\" }\",\"id\":\"5\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIObject\",\"type\":\"html.select\"}}],\"timeout\":10}",
												  "/10.5.2/EdinSP.testsuite",
												  null,
												  "<default>");



		return stepBatch;
	}

	// MoebStepBatch_Decl.template
	public MoebStepBatchAction applicationContext_7(IContainer parent) {
		MoebStepBatchAction stepBatch = new MoebStepBatchAction(parent,
												  "Click on Submit button whose Content is PROCEED - VB_Bridged",
												  "A1EDEFBC9407E98BD3F5E66138363330",
												  "{\"id\":\"0\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceTestSteps\",\"steps\":[{\"optionalStep\":false,\"uid\":\"A1EDEFBC9407E98BD3F5E66138363330\",\"preferredThinktime\":0,\"thinktime\":1832,\"isWeb\":false,\"description\":\"Think <i>1,832<\\/i> ms\",\"id\":\"1\",\"to_overriden\":false,\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceThinkTime\",\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0},{\"revealPref\":\"true\",\"description\":\"Click on <b>Submit button<\\/b> whose <b>Content<\\/b> is <b>PROCEED<\\/b>\",\"to_overriden\":false,\"skipScrollingIniOSDF\":false,\"application_uid\":\"43f09c67-a6d4-410a-a9f2-fdb4f20fa9ab\",\"timeout\":0,\"screenshotPref\":\"ALL\",\"optionalStep\":false,\"uid\":\"A1EDEFBC9407E98BD3F5E66138363330\",\"isWeb\":true,\"action\":{\"id\":\"3\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceAction\",\"type\":\"onclick\",\"parameters\":[{\"name\":\"enableasyncaction\",\"id\":\"4\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TBoolean\",\"value\":\"false\"}]},\"sync_policy\":\"DEFAULT\",\"id\":\"2\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIActionStep\",\"object\":{\"identifier\":{\"parameter\":{\"name\":\"content\",\"id\":\"7\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceParameter\",\"type\":\"TString\",\"value\":\"PROCEED\"},\"name\":\"content\",\"id\":\"6\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceId\",\"operator\":\"TEquals\"},\"propList\":\"{ \\\"Coordinates$array$\\\": \\\"top:455;left:1159;bottom:483;right:1275\\\", \\\"Geometry\\\": { \\\"height\\\": 28, \\\"width\\\": 116, \\\"x\\\": 1159, \\\"y\\\": 455 }, \\\"Style$array$\\\": \\\"background-color:rgb(160, 160, 160);background-repeat:repeat;color:rgb(255, 255, 255);font-family:MS Shell Dlg \\\\\\\\32 ;font-size:14.4px;font-style:normal;font-weight:400;line-height:normal;margin-bottom:0px;margin-left:20px;margin-right:0px;margin-top:0px;opacity:1;padding-bottom:1px;padding-left:4px;padding-right:4px;padding-top:1px;text-align:center;text-decoration:rgb(255, 255, 255);visibility:visible;z-index:auto;\\\", \\\"background-color\\\": \\\"A0A0A0FF\\\", \\\"background-repeat\\\": \\\"repeat\\\", \\\"bottom\\\": 483, \\\"class\\\": \\\"btn\\\", \\\"color\\\": \\\"FFFFFFFF\\\", \\\"content\\\": \\\"PROCEED\\\", \\\"domainName\\\": \\\"html\\\", \\\"enabled\\\": true, \\\"exist\\\": true, \\\"font-family\\\": \\\"MS Shell Dlg \\\\\\\\32 \\\", \\\"font-size\\\": \\\"14.4\\\", \\\"font-style\\\": \\\"normal\\\", \\\"font-weight\\\": \\\"400\\\", \\\"left\\\": 1159, \\\"line-height\\\": \\\"normal\\\", \\\"margin-bottom\\\": 0, \\\"margin-left\\\": 20, \\\"margin-right\\\": 0, \\\"margin-top\\\": 0, \\\"opacity\\\": \\\"1\\\", \\\"padding-bottom\\\": 1, \\\"padding-left\\\": 4, \\\"padding-right\\\": 4, \\\"padding-top\\\": 1, \\\"proxyClass\\\": \\\"HtmlSubmitProxy\\\", \\\"proxyName\\\": \\\"inputsubmit\\\", \\\"right\\\": 1275, \\\"tagname\\\": \\\"input\\\", \\\"text-align\\\": \\\"center\\\", \\\"text-decoration\\\": \\\"FFFFFFFF\\\", \\\"top\\\": 455, \\\"type\\\": \\\"SUBMIT\\\", \\\"value\\\": \\\"PROCEED\\\", \\\"visibility\\\": \\\"visible\\\", \\\"visible\\\": true, \\\"xpath\\\": \\\"298\\\", \\\"xpathProp\\\": \\\"\\\\\\/\\\\\\/form[@id='bookingForm']\\\\\\/table\\\\\\/tbody\\\\\\/tr\\\\\\/td[3]\\\\\\/table\\\\\\/tbody\\\\\\/tr\\\\\\/td\\\\\\/input[7]\\\", \\\"z-index\\\": \\\"auto\\\" }\",\"id\":\"5\",\"declaredClass\":\"com.ibm.rational.test.lt.core.moeb.model.transfer.testscript.DeviceUIObject\",\"type\":\"html.inputsubmit\"}}],\"timeout\":10}",
												  "/10.5.2/EdinSP.testsuite",
												  null,
												  "<default>");



		return stepBatch;
	}

}
