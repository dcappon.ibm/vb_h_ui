/**@generated
 * WARNING � Changes you make to this file may be lost.
 *           File is generated and may be re-generated without warning.
 * @RPT-Core-generated Version 8.6
 */
package test;

import com.ibm.rational.test.lt.kernel.action.IContainer;
import com.ibm.rational.test.lt.kernel.data.FeatureOptionsDataAreaCreation;
import java.util.HashMap;

@SuppressWarnings("all")
public class Tests_CompoundTest_A1EDEFBC07AA12F0D3F5E66138363330
		extends com.ibm.rational.test.lt.kernel.action.impl.CompoundTest {

	public Tests_CompoundTest_A1EDEFBC07AA12F0D3F5E66138363330(IContainer parent, String name) {
		super(parent, "Tests", "A1EDEFBC07AA12F0D3F5E66138363330");
	}

	public void execute() {

		this.add(SeleniumTest_1(this));
		this.add(new test.EdinSP_Test_A1EDEFBC9311F0B0D3F5E66138363330(this, "A1EDEFBCB4835770D3F5E66138363330") {
			public void execute() {
				this.setRtbEnabled(false);
				super.execute();
			}
		});

		super.execute();
	}

	private com.ibm.rational.test.rtw.se.codegen.lib.SeleniumScriptExecutor SeleniumTest_1(IContainer parent) {
		com.ibm.rational.test.rtw.se.codegen.lib.SeleniumScriptExecutor selExec = new com.ibm.rational.test.rtw.se.codegen.lib.SeleniumScriptExecutor(
				this, "BarcSP");
		selExec.setProjectPath("Selenium");
		selExec.setSourcePath("sel.BarcSP");
		selExec.setIsJunit(false);
		selExec.setExecutionArgs("");
		return selExec;
	}

}